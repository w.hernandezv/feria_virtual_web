from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
import json

##### Negocio #####
try:
    from Negocio.Usuario import Usuario
    from Negocio.Cliente import Cliente
    from Negocio.Producto import Producto
    from Negocio.Pedido import Pedido
    from Negocio.Subasta import Subasta
    from Negocio.DetallePedido import DetallePedido
    from Negocio.DetalleEscogido import DetalleEscogido
    from Negocio.DetalleOferta import DetalleOferta
    from Negocio.Productor import Productor
    from Negocio.Calidad import Calidad
    from Negocio.ProductoOfrecido import ProductoOfrecido
    from Negocio.Direccion import Direccion
    from Negocio.Paginacion import Paginacion
    from Negocio.Decorators import *
    from Negocio.Bid import Bid
    from Negocio.Transportista import Transportista
    from Negocio.Pregunta import Pregunta
    from Negocio.Encuesta import Encuesta

    print('Se pudo conectar al servicio')
except Exception as e:
    print('No se pudo conectar al servicio')
    print(e)

####### Email ######
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
####################

###################
# Vistas globales #
###################

def index(request):
    try:
        user = Usuario(dictionary=request.session.get('user'))
        if user.is_token_valid():
            context = {}
            current_page = request.GET.get('page')

            # Cliente
            if user.tipo_usuario == 1:
                #if request.GET.get('finalizados') == '':
                #    pedidos = Pedido.get_pedidos_finalizados(user.id)
                #    context['finalizados'] = True
                #else:
                #    pedidos = Pedido.get_pedidos_activos(user.id)
                #context['page'] = Paginacion.get_page(pedidos, current_page, 5)
                
                #return render(request, 'cliente/index.html', context)
                if request.GET.get('categoria'):
                    productos = Producto.get_productos_by_categoria(request.GET.get('categoria'))
                    context['categoria'] = int(request.GET.get('categoria'))
                else:
                    productos = Producto.all()
                    context['categoria'] = 3
                context['page'] = Paginacion.get_page(productos, current_page, 12)
                context['calidades'] = Calidad.all()
                return render(request, 'cliente/index.html', context)

            # Productor
            elif user.tipo_usuario == 2:
                if request.GET.get('finalizados') == '':
                    pedidos = Pedido.get_pedidos_finalizados_p(user.id)
                    context['finalizados'] = True
                elif request.GET.get('ofertados') == '':
                    pedidos = Pedido.get_pedidos_ofertados(user.id)
                    context['ofertados'] = True
                else:
                    pedidos = Pedido.get_pedidos_pendientes(user.id)
                    context['pendientes'] = True
                context['page'] = Paginacion.get_page(pedidos, current_page, 5)
                return render(request, 'productor/index.html', context)

            # Transportista
            elif user.tipo_usuario == 3:
                if request.GET.get('finalizadas') == '':
                    subastas = Subasta.get_subastas_finalizadas(user.id)
                    context['finalizadas'] = True
                else:
                    subastas = Subasta.get_subastas_activas(user.id)
                    context['activas'] = True
                context['page'] = Paginacion.get_page(subastas, current_page, 5)
                return render(request, 'transportista/index.html', context)

            # Ejecutivo
            elif user.tipo_usuario == 6:
                return render(request, 'ejecutivo/index.html', context)
            else:
                return redirect('cerrarsesion')
        else:
            return render(request, 'login.html')
    except Exception as e:
        print(repr(e))
        print(e)
        return render(request, 'login.html')


def authenticate(request):
    try:
        email = request.POST.get('email','')
        password = request.POST.get('password','')
        
        user = Usuario(email = email, password = password)
        if user.iniciar_sesion():
            request.session['user'] = user.to_dict()
            request.session['cart'] = []
            return redirect('index')
        else:
            messages.error(request, "Datos ingresados no son validos. Si has olvidado tu contraseña, puedes reestablecerla haciendo click <a href='/passwordreset/' %}' class='alert-link'>aquí</a>")
            return redirect('index')
    except Exception as e:
        print(e)
        return redirect('index')


def cerrar_sesion(request):
    request.session.flush()
    return redirect('index')


def signup(request):
    user = Usuario(dictionary=request.session.get('user'))
    if user.is_token_valid():
        return redirect('index')
    else:
        return render(request, 'signup.html')


def registrar(request):
    user = Usuario
    user.rut = request.POST.get('rut')
    user.nombre = request.POST.get('nombre')
    user.apellido = request.POST.get('apellido')
    user.email = request.POST.get('email')
    user.password = request.POST.get('password')
    user.tipo_usuario = 1
    user.telefono = request.POST.get('telefono')
    
    cliente = Cliente(usuario = user)

    context = {}

    # Las contraseñas ingresadas no eran iguales
    if request.POST.get('password') != request.POST.get('password2'):
        return redirect('signup')

    if cliente.registrar():
        messages.success(request, "Tu cuenta ha sido registrada exitosamente.")
        return redirect('index')
    else:
        return redirect('index')


def password_reset(request):
    user = Usuario(dictionary=request.session.get('user'))
    if user.is_token_valid():
        return redirect('index')
    else:
        return render(request, 'password_reset.html')


def password_reset_send(request):
    usuario = Usuario(email=request.POST.get('email'))
    usuario.get_reset_token()

    subject = 'FeriaVirtual - Reestablecer tu contraseña'
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [request.POST.get('email')]

    plain_message = 'Para reestablecer tu contraseña ingresa al siguiente link: http://' + request.get_host() + '/password_reset/reset/' + usuario.reset_token
    send_mail(subject, plain_message, email_from, recipient_list)

    messages.info(request, "Hemos enviado un mensaje a tu correo con indicaciones para reestablecer tu contraseña.")

    return redirect('password_reset')


def password_reset_form(request, token):
    user = Usuario(reset_token=token)
    if user.is_reset_token_valid():
        return render(request, 'passwd_reset.html', {'token':token})
    else:
        return redirect('index')


def password_reset_done(request, token):
        user = Usuario(reset_token = token, password=request.POST.get('password'))
        user.reset_password()
        return redirect('index')


@login_required()
def profile(request):
    user = Usuario(dictionary=request.session.get('user'))
    return render(request, 'profile.html')
    

##################
# Vistas cliente #
##################

@login_required(tipo_usuario = 1)
def pedidos(request):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    current_page = request.GET.get('page')
    if request.GET.get('finalizados') == '':
        pedidos = Pedido.get_pedidos_finalizados(user.id)
        context['finalizados'] = True
    else:
        pedidos = Pedido.get_pedidos_activos(user.id)
    context['page'] = Paginacion.get_page(pedidos, current_page, 5)
    return render(request, 'cliente/pedidos.html', context)


@login_required(tipo_usuario = 1)
def cart(request):
    context = {}
    context['calidades'] = Calidad.all()

    cart = request.session.get('cart')
    print(Calidad.all())
    print(cart)

    return render(request, 'cliente/cart.html', context)


@login_required(tipo_usuario = 1)
def cart_add(request):
    
    producto_id = int(request.POST.get('producto'))
    producto_nombre = request.POST.get('productoNombre')

    calidad = request.POST.get('calidad')
    calidad = calidad.split('-')
    calidad_id = int(calidad[0])
    calidad_nombre = calidad[1]

    toneladas = int(request.POST.get('toneladas'))

    product = {"Producto":{"Id":producto_id, "Nombre":producto_nombre}, "Calidad":{"Id":calidad_id, "Nombre":calidad_nombre}, "Toneladas":toneladas}



    cart = request.session.get('cart')

    if len(cart) == 0:
        cart.append(product)
        request.session['cart'] = cart
        return redirect('cart')

    producto_en_carro = False
    for i in cart:
        if product['Producto']['Id'] == i['Producto']['Id']: 
            producto_en_carro = True
            break
        else:
            pass

    if producto_en_carro:
        pass
    else:
        cart.append(product)

    request.session['cart'] = cart
    return redirect('cart')


@login_required(tipo_usuario = 1)
def cart_remove(request):
    cart = request.session.get('cart')

    cart.pop(int(request.POST.get('index')))

    request.session['cart'] = cart

    return redirect('cart')


@login_required(tipo_usuario = 1)
def previo_envio(request):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['direcciones'] = Direccion.get_direcciones(user.id)
    return render(request, 'cliente/previo_envio.html', context)


@login_required(tipo_usuario = 1)
def pedido(request, id):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['pedido'] = Pedido.get_pedido(id)
    context['detalle'] = DetallePedido.getDetalle(id)
    return render(request, 'cliente/pedido.html', context)


@login_required(tipo_usuario = 1)
def nuevopedido(request):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['productos'] = Producto.all()
    context['calidades'] = Calidad.all()
    context['direcciones'] = Direccion.get_direcciones(user.id)
    return render(request, 'cliente/nuevo_pedido.html', context)


@login_required(tipo_usuario = 1)
def enviarpedido(request):
    user = Usuario(dictionary=request.session.get('user'))
    #form_value = request.POST.copy()
    #productos = form_value.getlist('hidden_producto[]')
    #calidades = form_value.getlist('hidden_calidad[]')
    #cantidades = form_value.getlist('hidden_cantidad[]')

    #res = []
    #for e in range(0, len(productos)):
    #    res.append({"Producto":{"Id":productos[e]}, "Calidad":{"Id":calidades[e]}, "Toneladas":cantidades[e]})

    res = []



    pedido = {
        "Direccion":{
            "Id":request.POST.get('direccion')
        },
        "Cliente":{
            "Id":user.id
        }
    }
    #pedido['DetallePedido'] = res
    pedido['DetallePedido'] = request.session.get('cart')

    if Pedido.crear_pedido(pedido):
        request.session['cart'] = []
        messages.success(request, "Tu pedido ha sido enviado exitosamente.")
    else:
        messages.error(request, "Ocurrio un error al enviar tu pedido, intenta nuevamente mas tarde.")
    return redirect('pedidos')


@login_required(tipo_usuario = 1)
def producto(request, id_pedido, id_producto):
    pedido = Pedido.get_pedido(id_pedido)
    producto = Producto.get_producto(id_producto)
    return render(request, 'cliente/pedido_producto.html', {'pedido':pedido,'producto':producto})
    

@login_required(tipo_usuario = 1)
def direcciones(request):
    try:
        user = Usuario(dictionary=request.session.get('user'))

        file = open('feriavirtual/static/json/regiones.json', encoding='utf-8')
        data = file.read()
        file.close()
        regiones = json.loads(data)
        context = {}
        context['direcciones'] = Direccion.get_direcciones(user.id)
        context['regiones'] = regiones
        return render(request, 'cliente/direcciones.html', context)
    except Exception as e:
        print(e)
        return redirect('index')


@login_required(tipo_usuario = 1)
def agregar_direccion(request):
    try:
        user = Usuario(dictionary=request.session.get('user'))
        # Agregar direccion
        nombre = request.POST.get('nombre')
        pais = request.POST.get('pais')
        region = request.POST.get('region')
        codigopostal = request.POST.get('codigopostal')
        detalle = request.POST.get('detalle')
        direccion = {"Productor":{"Id":user.id},"Nombre":nombre,"Pais":pais,"Region":region,"CodigoPostal":codigopostal,"Detalle":detalle}
        Direccion.agregar_direccion(direccion)
        return redirect('direcciones')
    except Exception as e:
        print(e)
        return redirect('direcciones')


@login_required(tipo_usuario = 1)
def quitar_direccion(request):
    try:
        user = Usuario(dictionary=request.session.get('user'))
    
        # Quitar direccion
        idDireccion = request.POST.get('direccion')
        if Direccion.quitar_direccion(idDireccion):
            return redirect('direcciones')
        else:
            messages.error(request, "Dirección no pudo ser eliminada, ya que existen pedidos asociados a esta.")
            return redirect('direcciones')
    except Exception as e:
        return redirect('direcciones')


@login_required(tipo_usuario = 1)
def confirmar_pedido(request, id_pedido):
    if Pedido.confirmar_pedido(id_pedido):
        messages.success(request, 'Tu pedido ha sido enviado para iniciar el proceso de subasta que determinara a la empresa transportista.')
        return redirect('pedidos')
    else:
        messages.error(request, 'Tu pedido no pudo ser confirmado, intenta nuevamente mas tarde.')
        return redirect('pedido', id = id_pedido)


@login_required(tipo_usuario = 1)
def cancelar_pedido(request, id_pedido):
    Pedido.cancelar_pedido(id_pedido)
    return redirect('pedidos')


@login_required(tipo_usuario = 1)
def confirmar_recepcion(request, id_pedido):
    Pedido.confirmar_recepcion(id_pedido)
    return redirect('pedido', id = id_pedido)


@login_required(tipo_usuario = 1)
def pago_pedido(request, id_pedido):
    context = {}
    context['pedido'] = Pedido.get_pedido(id_pedido)
    return render(request, 'cliente/pago_pedido.html', context)


@login_required(tipo_usuario = 1)
def realizar_pago(request, id_pedido):
    Pedido.realizar_pago(id_pedido)
    messages.success(request, f"El pago para tu pedido #{id_pedido} ha sido realizado exitosamente.")
    return redirect('post_pago', id_pedido = id_pedido)


@login_required(tipo_usuario = 1)
def post_pago(request, id_pedido):
    context = {}
    context['pedido'] = {'Id':id_pedido}
    return render(request, 'cliente/post_pago.html', context)


@login_required(tipo_usuario = 1)
def encuesta(request, id_pedido):
    context = {}
    context['pedido'] = id_pedido
    context['preguntas'] = Pregunta.all()
    return render(request, 'cliente/encuesta.html', context)


@login_required(tipo_usuario = 1)
def enviar_encuesta(request, id_pedido):
    
    encuesta = {"Id":id_pedido, "Respuestas":[]}

    preguntas = Pregunta.all()

    for pregunta in preguntas:
        _id = pregunta['Id']
        a = request.POST.get(f'pregunta{_id}')
        encuesta['Respuestas'].append({"Pregunta":{"Id":_id}, "Puntaje":a })

    if Encuesta.enviar(encuesta):
        messages.success(request, "Tus respuestas fueron enviadas, gracias por preferirnos.")
    else:
        messages.error(request, "Ocurrio un error al enviar tus respuestas.")
        
    return redirect('pedido', id = id_pedido)

@login_required(tipo_usuario = 1)
def pago_go(request, id_pedido):
    metodo_pago = request.POST.get('metodopago')
    context = {}

    context['pedido'] = Pedido.get_pedido(id_pedido)
    
    if metodo_pago == 'deb':
        return render(request, 'cliente/pago_debito.html', context)
    else:
        return render(request, 'cliente/pago_credito.html', context)



####################
# Vistas productor #
####################

@login_required(tipo_usuario = 2)
def productor_pedido(request, id):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['pedido'] = Pedido.get_pedido_productor(id, user.id)
    context['pedidoId'] = id
    return render(request, 'productor/pedido.html', context)


@login_required(tipo_usuario = 2)
def productor_ofertar(request, id_pedido, id_producto, id_calidad):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['detalle'] = DetallePedido.getDetalleById(id_pedido, id_producto, id_calidad)
    context['calidades'] = Calidad.all()
    context['pedidoId'] = id_pedido
    return render(request, 'productor/ofertar.html', context)


@login_required(tipo_usuario = 2)
def productor_enviar_oferta(request):
    detalle = DetalleOferta()
    detalle.IdPedido = request.POST.get('pedido')
    detalle.IdProducto = request.POST.get('producto')
    detalle.IdCalidad = request.POST.get('calidad')
    detalle.IdProductor = request.session.get('user')['id']
    detalle.Toneladas = request.POST.get('toneladas')
    detalle.Precio = request.POST.get('precio')

    if detalle.ofertar():
        print('se pudo ofertar')
    else:
        print('error al ofertar')

    return redirect('productor_pedido', id=request.POST.get('pedido'))


@login_required(tipo_usuario = 2)
def add_producto_ofrecido(request):
    try:
        Productor.addProductoOfrecido(
            request.session.get('user')['id'],
            request.POST.get('producto'),
            request.POST.get('calidad')
        )
        return redirect('productos_ofrecidos')
    except:
        return redirect('productos_ofrecidos')
        

@login_required(tipo_usuario = 2)
def remove_producto_ofrecido(request):
    try:
        Productor.removeProductoOfrecido(
            request.session.get('user')['id'],
            request.POST.get('producto'),
            request.POST.get('calidad')
        )
        return redirect('productos_ofrecidos')
    except:
        return redirect('productos_ofrecidos')


@login_required(tipo_usuario = 2)
def productos_ofrecidos(request):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['productos'] = Producto.all()
    context['productosOfrecidos'] = Productor.productosOfrecidos(user.id)
    context['productosNoOfrecidos'] = Productor.productosNoOfrecidos(user.id)
    context['calidades'] = Calidad.all()
    return render(request, 'productor/productos_ofrecidos.html', context)
    

@login_required(tipo_usuario = 2)
def toggle_habilitado(request):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    ProductoOfrecido.ToggleHabilitado(request.POST.get('producto'), user.id, request.POST.get('calidad'))
    return redirect('productos_ofrecidos')
    
@login_required(tipo_usuario = 2)
def productor_quitar_oferta(request):
    user = Usuario(dictionary=request.session.get('user'))
    id_pedido = request.POST.get('pedido')
    id_producto = request.POST.get('producto')
    id_calidad = request.POST.get('calidad')

    DetalleOferta.quitar_oferta(id_pedido, id_producto, id_calidad, user.id)

    return redirect('productor_pedido', id=request.POST.get('pedido'))

########################
# Vistas transportista #
########################

@login_required(tipo_usuario = 3)
def subasta(request, id):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['subasta'] = Subasta.get_subasta(id)
    context['pedido'] = Pedido.get_pedido(context['subasta']['IdPedido'])
    context['detalle'] = DetallePedido.getDetalle(context['subasta']['IdPedido'])

    import json
    print(json.dumps(context['detalle']))
    context['your_bid'] = Bid.get_your_bid(id, user.id)
    return render(request, 'transportista/subasta.html', context)


@login_required(tipo_usuario = 3)
def toggle_retirar(request):

    id_pedido = request.POST.get('pedido')
    id_producto = request.POST.get('producto')
    id_productor = request.POST.get('productor')
    id_calidad = request.POST.get('calidad')
    Subasta.toggle_retirar(id_pedido, id_producto, id_productor, id_calidad)

    return redirect('subasta', id = request.POST.get('subasta'))


@login_required(tipo_usuario = 3)
def confirmar_retiro(request):
    id_pedido = request.POST.get('pedido')
    Pedido.confirmar_retiro(id_pedido)
    return redirect('subasta', id = request.POST.get('subasta'))

    
@login_required(tipo_usuario = 3)
def confirmar_entrega(request):
    id_pedido = request.POST.get('pedido')
    Pedido.confirmar_entrega(id_pedido)
    return redirect('subasta', id = request.POST.get('subasta'))


@login_required(tipo_usuario = 3)
def transportista_ofertar(request, id_subasta):
    Bid.bid(id_subasta, request.session.get('user')['id'], request.POST.get('precio'))
    return redirect('subasta', id = id_subasta)


####################
# Vistas ejecutivo #
####################

@login_required(tipo_usuario = 6)
def pedido_ejecutivo(request, id_pedido):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['pedido'] = Pedido.get_pedido(id_pedido)
    context['productores'] = Pedido.get_posibles_productores(id_pedido)
    return render(request, 'ejecutivo/pedido.html', context)


@login_required(tipo_usuario = 6)
def ejecutivo_pedidos(request):
    current_page = request.GET.get('page')
    pedidos = Pedido.get_pedidos_en_seleccion()
    context = {}
    context['page'] = Paginacion.get_page(pedidos, current_page, 5)

    return render(request, 'ejecutivo/pedidos.html', context)


@login_required(tipo_usuario = 6)
def invitar_pedido(request, id_pedido):

    inv = request.POST.getlist('invitados')
    invitados = []
    for i in inv:
        invitados.append({"Id":i})

    Pedido.invitar(id_pedido, invitados)

    return redirect('pedido_ejecutivo', id_pedido = id_pedido)


@login_required(tipo_usuario = 6)
def ejecutivo_subastas(request):
    current_page = request.GET.get('page')
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    if request.GET.get('finalizadas') == '':
        subastas = Subasta.get_subastas_finalizadas()
        context['finalizadas'] = True
    else:
        subastas = Subasta.get_subastas_activas()
        context['activas'] = True
    context['page'] = Paginacion.get_page(subastas, current_page, 5)
    return render(request, 'ejecutivo/subastas.html', context)


@login_required(tipo_usuario = 6)
def ejecutivo_subasta(request, id_subasta):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['subasta'] = Subasta.get_subasta(id_subasta)
    context['pedido'] = Pedido.get_pedido(context['subasta']['IdPedido'])
    context['detalle'] = DetallePedido.getDetalle(context['subasta']['IdPedido'])
    context['bids'] = Bid.get_bids(id_subasta)
    context['transportistas'] = Transportista.All()
    return render(request, 'ejecutivo/subasta.html', context)


@login_required(tipo_usuario = 6)
def invitar_subasta(request, id_subasta):

    inv = request.POST.getlist('invitados')
    invitados = []
    for i in inv:
        invitados.append({"Id":i})

    Subasta.invitar(id_subasta, invitados)

    return redirect('ejecutivo_subasta', id_subasta = id_subasta)


@login_required(tipo_usuario = 6)
def subasta_participantes(request, id_subasta):
    context = {}
    context['subasta'] = Subasta.get_subasta(id_subasta)
    context['participantes'] = Subasta.get_invitados(id_subasta)

    return render(request, 'ejecutivo/subasta_participantes.html', context)


@login_required(tipo_usuario = 6)
def pedido_participantes(request, id_pedido):
    context = {}
    context['pedido'] = Pedido.get_pedido(id_pedido)
    context['participantes'] = Pedido.get_invitados(id_pedido)

    return render(request, 'ejecutivo/pedido_participantes.html', context)


@login_required(tipo_usuario = 6)
def escoger_bid(request):
    id_subasta = request.POST.get('subasta')
    id_transportista = request.POST.get('transportista')
    Bid.escoger_bid(id_subasta, id_transportista)
    return redirect('ejecutivo_subasta', id_subasta = id_subasta)


@login_required(tipo_usuario = 6)
def finalizar_subasta(request):
    id_subasta = request.POST.get('subasta')
    Subasta.finalizar_subasta(id_subasta)
    return redirect('ejecutivo_subasta', id_subasta = id_subasta)


@login_required(tipo_usuario = 6)
def pedido_producto_ejecutivo(request, id_pedido, id_producto, id_calidad):
    user = Usuario(dictionary=request.session.get('user'))
    context = {}
    context['pedido'] = Pedido.get_pedido(id_pedido)
    context['producto'] = Producto.get_producto(id_producto)
    context['ofertas'] = DetalleOferta.getOfertas(id_pedido, id_producto, id_calidad)
    context['detalle_pedido'] = DetallePedido.getDetalleById(id_pedido, id_producto, id_calidad)
    return render(request, 'ejecutivo/pedido_producto.html', context)


@login_required(tipo_usuario = 6)
def completar_pedido(request, id_pedido):
    if Pedido.completar_pedido(id_pedido):
        messages.success(request, "El pedido ha sido enviado al cliente para su confirmación.")
        return redirect('ejecutivo_pedidos')
    else:
        messages.error(request, "El pedido no pudo ser confirmado.")
        return redirect('pedido_ejecutivo', id_pedido = id_pedido)


@login_required(tipo_usuario = 6)
def escoger_oferta(request):
    id_pedido = request.POST.get('id_pedido')
    id_producto = request.POST.get('id_producto')
    id_calidad = request.POST.get('id_calidad')
    id_productor = request.POST.get('id_productor')
    toneladas = request.POST.get('toneladas')

    if DetalleEscogido.escoger_oferta(id_pedido, id_producto, id_calidad, id_productor, toneladas):
        messages.success(request, "Oferta escogida.")
        return redirect('pedido_ejecutivo', id_pedido = id_pedido)
    else:
        messages.error(request, "No se pudo escoger la oferta.")
        return redirect('pedido_producto_ejecutivo', id_pedido = id_pedido, id_producto = id_producto, id_calidad = id_calidad)

@login_required(tipo_usuario = 6)
def habilitar_pago(request, id_pedido):

    if Pedido.habilitar_pago(id_pedido):
        messages.success(request, f"El pago para el pedido #{id_pedido} ha sido habilitado.")
    else:
        messages.error(request, f"Ocurrio un error al intentar habilitar el pago para el pedido #{id_pedido}.")
    return redirect('pedido_ejecutivo', id_pedido = id_pedido)
