from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class DetallePedido:

    def getDetalle(id):
        detalle = json.loads(client.service.GetDetallePedido(id))
        return detalle

    def getDetalleById(idPedido, idProducto, idCalidad):
        detalle = json.loads(client.service.GetDetallePedidoById(idPedido, idProducto, idCalidad))
        return detalle
