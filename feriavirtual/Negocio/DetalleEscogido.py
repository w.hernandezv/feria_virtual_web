from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class DetalleEscogido:
    
    def getTotalDetalle(id_pedido, id_producto):
        total = client.service.GetTotalDetalle(id_pedido, id_producto)
        return total

    def escoger_oferta(id_pedido, id_producto, id_calidad, id_productor, toneladas):
        if(client.service.EscogerOferta(id_pedido, id_producto, id_calidad, id_productor, toneladas)):
            return True
        else:
            return False