from suds.client import Client
import json
from django.conf import settings
from django.core.paginator import Paginator

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Encuesta:

    def enviar(encuesta):
        encuesta = json.dumps(encuesta)
        return client.service.EnviarEncuesta(encuesta)
