from suds.client import Client
import json
from django.conf import settings
from django.core.paginator import Paginator

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Pregunta:

    def all():
        return json.loads(client.service.GetPreguntas())