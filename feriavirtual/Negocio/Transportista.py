from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Transportista:
    def __init__(self, usuario = None, direccion = None):
        self.usuario = usuario
        self.direccion = direccion

    def All():
        return json.loads(client.service.GetAllTransportistas())