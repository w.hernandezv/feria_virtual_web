from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Producto:

    def __init__(self, id, nombre):
        self.id = id
        self.nombre = nombre

    # Get all products
    def all():
        productos = json.loads(client.service.GetProductos())
        return productos

    def get_producto(id):
        producto = json.loads(client.service.GetProducto(id))
        return producto

    def get_productos_by_categoria(id_categoria):
        return json.loads(client.service.GetProductosByCategoria(id_categoria))
