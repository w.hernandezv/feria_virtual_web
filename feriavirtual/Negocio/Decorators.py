from Negocio.Usuario import Usuario
from django.shortcuts import redirect
from functools import wraps

def login_required(redirect_url = 'index', tipo_usuario = None):
    def decorator(func):
        @wraps(func)
        def inner(request, *args, **kwargs):
            user = Usuario(dictionary=request.session.get('user'))
            if user.is_token_valid():
                if tipo_usuario is None:
                    return func(request, *args, **kwargs)
                if user.tipo_usuario == tipo_usuario:
                    return func(request, *args, **kwargs)
            return redirect(redirect_url)
        return inner
    return decorator
