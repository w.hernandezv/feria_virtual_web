from django.core.paginator import Paginator

class Paginacion:
    
    def get_page(dictionary = None, pageNumber = None, objPerPage = 5):
        paginator = Paginator(dictionary, objPerPage)
        page = paginator.get_page(pageNumber)
        return page
