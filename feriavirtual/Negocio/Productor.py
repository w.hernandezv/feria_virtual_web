from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Productor:
    def __init__(self, usuario = None, direccion = None):
        self.usuario = usuario
        self.direccion = direccion

    def toJson(self):
        return json.dumps(self.__dict__)

    def productosOfrecidos(idProductor):
        productos = client.service.GetProductosOfrecidos(idProductor)
        productos = json.loads(productos)
        return productos

    def productosNoOfrecidos(idProductor):
        productos = client.service.GetProductosNoOfrecidos(idProductor)
        productos = json.loads(productos)
        return productos

    def addProductoOfrecido(idProductor, idProducto, idCalidad):
        return client.service.AddProductoOfrecido(idProductor, idProducto, idCalidad)

    def removeProductoOfrecido(idProductor, idProducto, idCalidad):
        return client.service.RemoveProductoOfrecido(idProductor, idProducto, idCalidad)

    def ofertar(pedido, producto, calidad, productor, toneladas, precio):
        data = 0