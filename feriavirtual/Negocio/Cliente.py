from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Cliente:
    def __init__(self, usuario = None, direccion = None):
        self.usuario = usuario
        self.direccion = direccion

    def registrar(self):
        string = json.dumps({
            "Rut":self.usuario.rut,
            "Nombre":self.usuario.nombre,
            "Apellido":self.usuario.apellido,
            "Email":self.usuario.email,
            "Password":self.usuario.password,
            "TipoUsuario":self.usuario.tipo_usuario,
            "Direccion":self.direccion,
            "Telefono":self.usuario.telefono})

        if client.service.RegistrarUsuario(string):
            return True
        else:
            return False

    def toJson(self):
        return json.dumps(self.__dict__)