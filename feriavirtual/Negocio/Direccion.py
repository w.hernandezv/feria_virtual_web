from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Direccion:

    def get_direcciones(idCliente):
        lista = json.loads(client.service.GetDirecciones(idCliente))
        return lista

    def quitar_direccion(idDireccion):
        return client.service.RemoveDireccion(idDireccion)

    def agregar_direccion(direccion):
        direccion = json.dumps(direccion)
        return client.service.AddDireccion(direccion)
