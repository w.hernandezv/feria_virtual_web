from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class ProductoOfrecido:
    
    def ToggleHabilitado(producto, productor, calidad):
        resultado = client.service.ToggleHabilitado(producto, productor, calidad)

        if resultado:
            return True
        else:
            return False

