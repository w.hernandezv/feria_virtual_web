from suds.client import Client
import json
from django.conf import settings
from django.core.paginator import Paginator

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Pedido:

    def __init__(self, id = 0, fecha = '', estado = '', direccion = '', usuario = 0, productos = None):
        self.id = id
        self.fecha = fecha
        self.estado = estado
        self.direccion = direccion
        self.usuario = usuario
        self.productos = productos

    # Retorna un pedido, buscando por id de pedido
    def get_pedido(id_pedido):
        pedido = json.loads(client.service.GetPedido(id_pedido))
        return pedido

    # Retorna pedidos relacionados a un cliente
    def getPedidos(id_cliente):
        pedidos = json.loads(client.service.GetPedidos(id_cliente))
        return pedidos

    def get_pedidos_activos(id_cliente):
        pedidos = json.loads(client.service.GetPedidosActivos(id_cliente))
        return pedidos

    # Retorna pedidos relacionados a un cliente
    def get_pedidos_finalizados(id_cliente):
        pedidos = json.loads(client.service.GetPedidosFinalizados(id_cliente))
        return pedidos

    def getAllPedidos():
        pedidos = json.loads(client.service.GetAllPedidos())
        return pedidos

    def crear_pedido(pedido):
        pedido = json.dumps(pedido)
        try:
            if client.service.CrearPedido(pedido):
                return True
            else:
                return False
        except:
            return False

    def get_pedidos_pendientes(idProductor):
        pedidos = json.loads(client.service.GetPedidosPendientes(idProductor))
        return pedidos

    def get_pedidos_ofertados(idProductor):
        pedidos = json.loads(client.service.GetPedidosOfertados(idProductor))
        return pedidos

    def get_pedidos_finalizados_p(idProductor):
        pedidos = json.loads(client.service.GetPedidosFinalizadosP(idProductor))
        return pedidos

    # Retorna el pedido incluyendo solo los productos que el productor ofrece
    def get_pedido_productor(idPedido, idProductor):
        detalle = json.loads(client.service.GetPedidoProductor(idPedido, idProductor))
        return detalle

    def get_pedidos_en_seleccion():
        pedidos = json.loads(client.service.GetPedidosEnSeleccion())
        return pedidos

    # Estado 'En selección' -> 'Esperando confirmación', trigger = usuario ejecutivo
    def completar_pedido(idPedido):
        return client.service.CompletarPedido(idPedido)

    # Estado 'Esperando confirmación' -> 'En subasta', trigger = usuario cliente
    # Inicia el proceso de subasta
    def confirmar_pedido(idPedido):
        return client.service.ConfirmarPedido(idPedido)

    def cancelar_pedido(idPedido):
        return client.service.CancelarPedido(idPedido)

    def get_invitados(id_pedido):
        return json.loads(client.service.GetInvitadosPedido(id_pedido))

    def get_posibles_productores(id_pedido):
        return json.loads(client.service.GetPosiblesProductores(id_pedido))

    def invitar(id_pedido, invitados):
        invitados = json.dumps(invitados)
        return client.service.InvitarPedido(id_pedido, invitados)

    def confirmar_retiro(id_pedido):
        return client.service.ConfirmarRetiro(id_pedido)

    def confirmar_entrega(id_pedido):
        return client.service.ConfirmarEntrega(id_pedido)

    def confirmar_recepcion(id_pedido):
        return client.service.ConfirmarRecepcion(id_pedido)

    def realizar_pago(id_pedido):
        return client.service.RealizarPago(id_pedido)

    def habilitar_pago(id_pedido):
        return client.service.HabilitarPago(id_pedido)
