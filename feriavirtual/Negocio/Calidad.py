from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Calidad:

    def __init__(self, id = None, descripcion = None):
        self.id = id
        self.descripcion = descripcion

    def all():
        calidades = client.service.GetCalidades()
        return json.loads(calidades)