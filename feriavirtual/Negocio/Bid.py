from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Bid:
    def bid(id_subasta, id_transportista, precio):
        if precio == '':
            precio = '0'
        oferta = {'Subasta':{'Id':id_subasta}, 'Transportista':{'Id':id_transportista}, 'Precio':precio}
        oferta = json.dumps(oferta)
        return client.service.Bid(oferta)
    
    def get_your_bid(id_subasta, id_transportista):
        return json.loads(client.service.GetYourBid(id_subasta, id_transportista))

    def get_bids(id_subasta):
        return json.loads(client.service.GetBids(id_subasta))

    def escoger_bid(id_subasta, id_transportista):
        return client.service.EscogerBid(id_subasta, id_transportista)