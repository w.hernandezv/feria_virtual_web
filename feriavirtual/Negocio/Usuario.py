from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Usuario:

    def __init__(self, id = 0, rut = None, nombre = None, apellido = None, email = None, password = None, tipo_usuario = None, token = None, telefono = 0, reset_token = None, dictionary = None):
        self.id = id
        self.rut = rut
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.password = password
        self.tipo_usuario = tipo_usuario
        self.token = token
        self.telefono = telefono
        self.reset_token = reset_token

        if dictionary is not None:
            for k, v in dictionary.items():
                setattr(self, k, v)

    def to_dict(self):
        a = json.dumps(self.__dict__)
        a = json.loads(a)
        return a

    def is_token_valid(self):
        if client.service.IsTokenValid(self.id, self.token):
            return True
        else:
            return False

    def iniciar_sesion(self):
        credenciales = {"Email":self.email, "Password":self.password}
        credenciales = json.dumps(credenciales)
        result = client.service.IniciarSesion(credenciales)
        result = json.loads(result)

        self.id = result['Id']
        self.token = result['Token']
        self.rut = result['Rut']
        self.nombre = result['Nombre']
        self.apellido = result['Apellido']
        self.email = result['Email']
        self.tipo_usuario = result['TipoUsuario']
        #print(result)
        if result['Message'] == 'Datos invalidos':
            return False
        else:
            return True

    def is_reset_token_valid(self):
        if client.service.IsResetTokenValid(self.reset_token):
            return True
        else:
            return False

    def reset_password(self):
        if client.service.ResetPassword(self.reset_token, self.password):
            return True
        else:
            return False

    def get_reset_token(self):
        self.reset_token = client.service.GetResetToken(self.email)

    