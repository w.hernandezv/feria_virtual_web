from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class Subasta:

    def get_subasta(id):
        subasta = json.loads(client.service.GetSubasta(id))
        return subasta

    def get_all_subastas():
        subastas = json.loads(client.service.GetAllSubastas())
        return subastas

    def get_subastas_activas(id_transportista = None):
        if id_transportista is not None:
            subastas = json.loads(client.service.GetSubastasActivasWBid(id_transportista))
        else:
            subastas = json.loads(client.service.GetSubastasActivas())
        return subastas

    def get_subastas_finalizadas(id_transportista = None):
        if id_transportista is not None:
            subastas = json.loads(client.service.GetSubastasFinalizadasWBid(id_transportista))
        else:
            subastas = json.loads(client.service.GetSubastasFinalizadas())
        return subastas

    def finalizar_subasta(id_subasta):
        return client.service.FinalizarSubasta(id_subasta)

    def get_invitados(id_subasta):
        return json.loads(client.service.GetInvitadosSubasta(id_subasta))

    def invitar(id_subasta, invitados):
        invitados = json.dumps(invitados)
        return client.service.InvitarSubasta(id_subasta, invitados)

    def toggle_retirar(id_pedido, id_producto, id_productor, id_calidad):
        return client.service.ToggleRetirar(id_pedido, id_producto, id_productor, id_calidad)