from suds.client import Client
import json
from django.conf import settings

# Cliente SOAP
wsdl = settings.WSDL
client = Client(wsdl)

class DetalleOferta:
    
    IdPedido = None
    IdProducto = None
    IdCalidad = None
    IdProductor = None
    Toneladas = None
    Precio = None

    def __init__(self):
        pass

    def ofertar(self):
        data = self.__dict__
        data = json.dumps(data)

        return client.service.ProductorOfertar(data)

    def getOfertas(id_pedido, id_producto, id_calidad):
        ofertas = client.service.GetOfertasPedidoProducto(id_pedido, id_producto, id_calidad)
        ofertas = json.loads(ofertas)
        return ofertas

    def quitar_oferta(id_pedido, id_producto, id_calidad, id_productor):
        return client.service.QuitarOferta(id_pedido, id_producto, id_calidad, id_productor)