"""feriavirtual URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('authenticate/', views.authenticate, name='authenticate'),
    path('cerrarsesion/', views.cerrar_sesion, name='cerrarsesion'),
    path('signup/', views.signup, name='signup'),
    path('signup/registrar/', views.registrar, name='registrar'),
    path('profile/', views.profile, name='profile'),
    path('passwordreset/', views.password_reset, name='passwordreset'),
    path('passwordreset/send/', views.password_reset_send, name='passwordreset_send'),
    path('passwordreset/reset/<str:token>/', views.password_reset_form, name='passwordreset_form'),
    path('passwordreset/done/<str:token>/', views.password_reset_done, name='passwordreset_done'),
    
    ##################
    # Vistas cliente #
    ##################
    path('pedidos/', views.pedidos, name='pedidos'),
    path('cart/', views.cart, name='cart'),
    path('cart/add/', views.cart_add, name='cart_add'),
    path('cart/remove/', views.cart_remove, name='cart_remove'),
    path('cart/previoenvio/', views.previo_envio, name='previo_envio'),
    path('direcciones/', views.direcciones, name='direcciones'),
    path('direcciones/agregar/', views.agregar_direccion, name='agregar_direccion'),
    path('direcciones/quitar/', views.quitar_direccion, name='quitar_direccion'),
    path('pedido/<int:id>/', views.pedido, name='pedido'),
    path('pedido/<int:id_pedido>/confirmarrecepcion/', views.confirmar_recepcion, name='confirmar_recepcion'),
    path('pedido/<int:id_pedido>/cancelar/', views.cancelar_pedido, name='cancelar_pedido'),
    path('pedido/nuevo/', views.nuevopedido, name='nuevopedido'),
    path('pedido/nuevo/enviar/', views.enviarpedido, name='enviarpedido'),
    path('pedido/<int:id_pedido>/producto/<int:id_producto>/', views.producto, name='producto'),
    path('pedido/<int:id_pedido>/confirmar/', views.confirmar_pedido, name='confirmar_pedido'),
    path('pedido/<int:id_pedido>/pago/', views.pago_pedido, name='pago_pedido'),
    path('pedido/<int:id_pedido>/pago/realizarpago/', views.realizar_pago, name='realizar_pago'),
    path('pedido/<int:id_pedido>/pagar/postpago/', views.post_pago, name='post_pago'),
    path('pedido/<int:id_pedido>/encuesta/', views.encuesta, name='encuesta'),
    path('pedido/<int:id_pedido>/encuesta/enviar/', views.enviar_encuesta, name='enviar_encuesta'),
    path('pedido/<int:id_pedido>/pago/go/', views.pago_go, name='pago_go'),


    
    ####################
    # Vistas productor #
    ####################
    path('productor/pedido/<int:id>/', views.productor_pedido, name='productor_pedido'),
    path('productor/pedido/<int:id_pedido>/producto/<int:id_producto>/calidad/<int:id_calidad>/ofertar/', views.productor_ofertar, name='productor_ofertar'),
    path('productor/enviar_oferta/', views.productor_enviar_oferta, name='productor_enviar_oferta'),
    path('add_producto_ofrecido/', views.add_producto_ofrecido, name='add_producto_ofrecido'),
    path('remove_producto_ofrecido', views.remove_producto_ofrecido, name='remove_producto_ofrecido'),
    path('productos_ofrecidos/', views.productos_ofrecidos, name='productos_ofrecidos'),
    path('toggle_habilitado/', views.toggle_habilitado, name='toggle_habilitado'),
    path('productor_quitar_oferta/', views.productor_quitar_oferta, name='productor_quitar_oferta'),
    
    ########################
    # Vistas transportista #
    ########################
    path('transportista/subasta/<int:id>/', views.subasta, name='subasta'),
    path('transportista/subasta/<int:id_subasta>/bid/', views.transportista_ofertar, name='transportista_ofertar'),
    path('transportista/subasta/toggleretirar/', views.toggle_retirar, name='toggle_retirar'),
    path('transportista/subasta/confirmarretiro/', views.confirmar_retiro, name='confirmar_retiro'),
    path('transportista/subasta/confirmarentrega/', views.confirmar_entrega, name='confirmar_entrega'),

    ####################
    # Vistas ejecutivo #
    ####################
    path('ejecutivo/pedido/<int:id_pedido>/', views.pedido_ejecutivo, name='pedido_ejecutivo'),
    path('ejecutivo/pedido/<int:id_pedido>/producto/<int:id_producto>/calidad/<int:id_calidad>/', views.pedido_producto_ejecutivo, name='pedido_producto_ejecutivo'),
    path('ejecutivo/pedido/<int:id_pedido>/completarpedido/', views.completar_pedido, name='completar_pedido'),
    path('ejecutivo/escogeroferta/', views.escoger_oferta, name='escoger_oferta'),
    path('ejecutivo/pedidos/', views.ejecutivo_pedidos, name='ejecutivo_pedidos'),
    path('ejecutivo/subastas/', views.ejecutivo_subastas, name='ejecutivo_subastas'),
    path('ejecutivo/subastas/<int:id_subasta>/', views.ejecutivo_subasta, name='ejecutivo_subasta'),
    path('ejecutivo/escogerbid/', views.escoger_bid, name='escoger_bid'),
    path('ejecutivo/finalizarsubasta/', views.finalizar_subasta, name='finalizar_subasta'),
    path('ejecutivo/subastas/<int:id_subasta>/invitar/', views.invitar_subasta, name='invitar_subasta'),
    path('ejecutivo/subastas/<int:id_subasta>/participantes/', views.subasta_participantes, name='subasta_participantes'),

    path('ejecutivo/pedido/<int:id_pedido>/invitar/', views.invitar_pedido, name='invitar_pedido'),
    path('ejecutivo/pedido/<int:id_pedido>/participantes/', views.pedido_participantes, name='pedido_participantes'),

    path('ejecutivo/pedido/<int:id_pedido>/habilitarpago/', views.habilitar_pago, name='habilitar_pago'),




]
